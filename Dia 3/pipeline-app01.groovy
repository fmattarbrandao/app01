pipeline {
    agent any

    environment { 
        registry = "fmattarbrandao/app01" 
        registryCredential = 'dockerhub_id' 
        dockerImage = '' 
    }

    stages {
        stage('Clone Repository') {
            steps {
                git url: 'https://gitlab.com/fmattarbrandao/app01.git'
            }
        }
        stage('Build Docker Image') {
            steps {
                script {
                    dockerImage = docker.build registry + ":develop"
                }
            }
        }
        stage('Send image to Docker Hub') {
            steps {
                script {
                    docker.withRegistry( '', registryCredential ) { 
                    dockerImage.push() 
                    }
                }
            }
        }
    	stage('Deploy') {
		    steps{
                    step([$class: 'AWSCodeDeployPublisher', 
                        applicationName: 'app01-application',
                        awsAccessKey: "AKIAITGIOWP4YRXNQVBQ",
                        awsSecretKey: "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",
                        credentials: 'awsAccessKey',
                        deploymentGroupAppspec: false,
                        deploymentGroupName: 'service',
                        deploymentMethod: 'deploy',
                        excludes: '',
                        iamRoleArn: '',
                        includes: '**',
                        pollingFreqSec: 15,
                        pollingTimeoutSec: 600,
                        proxyHost: '',
                        proxyPort: 0,
                        region: 'us-east-1',
                        s3bucket: 'cloud-app01', 
                        s3prefix: '', 
                        subdirectory: '',
                        versionFileName: '',
                        waitForCompletion: true])
            }
        }
        stage('Cleaning up') {
            steps {
                sh "docker rmi $registry:develop"
            }
        }
    }
}